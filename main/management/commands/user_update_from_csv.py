# -*- coding: utf-8 -*-
import csv
import os

from django.conf import settings
from django.core.management import BaseCommand
from django.contrib.auth import get_user_model
User = get_user_model()

from main.models import CompanyStructure


def cp1251_decoder(data):
    for line in data:
        yield line.decode('cp1251').encode('utf-8')

class Command(BaseCommand):
    u'''
        ФИО;Майл;Должность;Отдел/Департамент;Роль
    '''
    help = 'Use: ./manage.py user_update_from_csv'

    def handle(self, *args, **options):
        dr = os.path.join(settings.BASE_DIR, 'finter/fixtures/')
        with open(dr+'efcontacts.csv', 'rb') as csvfile:
            data = csv.reader(cp1251_decoder(csvfile))
            for row in data:
                fields = row[0].split(';')
                dp = fields[3].replace('\xc2\xa0', ' ').replace('  ', ' ')
                em = fields[1].split('\xc2\xa0')
                email = em[1] if len(em) > 1 else em[0]
                usr = User.objects.filter(email=email).last()
                if usr:
                    usr.post = fields[2]
                    if dp not in ('', ' ', 'NULL', '\xc2\xa0'):
                        cs = CompanyStructure.objects.filter(name=dp)
                        if cs.exists():
                            dp = cs.first()
                        else:
                            dp = CompanyStructure.objects.create(name=dp, order=999)
                        usr.department = dp
                    usr.save()
        return 'done'        