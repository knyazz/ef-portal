# -*- coding: utf-8 -*-
import ldap

from django.conf import settings
from django.core.management import BaseCommand

from main.backends import MYLDAPBackend


class Command(BaseCommand):
    help = 'WMS Xml-file parser daemon. Use: ./manage.py put_ldap_user_in_db'

    def handle(self, *args, **options):
        ldap.protocol_version = 3
        l = ldap.initialize(settings.AUTH_LDAP_SERVER_URI)
        l.simple_bind_s(settings.AUTH_LDAP_BIND_DN, settings.AUTH_LDAP_BIND_PASSWORD)
        lst1 = l.search_s(  "OU=VO Office,OU=Экспофорум,DC=expoforum,DC=ru", 
                            ldap.SCOPE_SUBTREE,
                            "(cn=*)",
                            ["sAMAccountName"]
        ) or []
        lst2 = l.search_s(  "OU=Ленэкспо,OU=Экспофорум,DC=expoforum,DC=ru", 
                            ldap.SCOPE_SUBTREE,
                            "(cn=*)",
                            ["sAMAccountName"]
        ) or []
        lst = [f[1].get('sAMAccountName', [None])[0] for f in lst1+lst2]
        myldapcon= MYLDAPBackend()
        for username in lst:
            if username:
                myldapcon.put_users_in_db(username)
        return 'done'
        