#coding: utf-8
from django.contrib.auth import get_user_model
User = get_user_model()
from django.db.models import Count
from django.shortcuts import redirect
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import TemplateView
from django.views.generic.list import ListView

from conversejs.models import XMPPAccount

from filestorage.models import Gallery
from infos.models import DocTemplate, Subsidairy

from .forms import WishForm, CommentForm#, AdForm
from .models import Ad, Book, Calendar, Contest, CompanyStructure
from .models import Direction, BirthDate, Question#, Vacation, 
from .models import News, NewsSliderItem, Quiz, QuizUserVote
from .models import Plan, SystemTunes, Newbie, EmplTraining
from .utils import slice_list

class ContextMixin(object):
    def get_context_data(self, **kwargs):
        ctx = super(ContextMixin, self).get_context_data(**kwargs)
        try:
            xmpp_account = XMPPAccount.objects.get(user=self.request.user.pk)
            ctx['user_jid'] = xmpp_account
        except XMPPAccount.DoesNotExist: pass
        ctx['ads'] = slice_list(Ad.objects.all())
        ctx['news'] = slice_list(News.objects.all())
        ctx['calendar_events'] = slice_list(Calendar.objects.all(), 5)
        ctx['clrs'] = ('blue','green','orange')
        ctx['birth_date'] = slice_list(BirthDate.objects.birth_users(), 3)
        ctx['newbies'] = slice_list(Newbie.objects.all(), 3)
        try:
            ctx['title'] = self.model._meta.verbose_name_plural
        except: pass
        return ctx


class IndexPage(ContextMixin, TemplateView):
    u''' Главная страница '''
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        ctx = super(IndexPage, self).get_context_data(**kwargs)
        ctx['newssliders'] = NewsSliderItem.objects.filter(is_active=True,
                                                            is_publish=True)
        return ctx
index = IndexPage.as_view()
calendar = IndexPage.as_view(template_name='calendar.html')
company_structure = IndexPage.as_view(template_name='company_structure.html')

class ListPage(ContextMixin, ListView):
    model = News
    paginate_by = 10
    template_name = 'list.html'  
news_list = ListPage.as_view()
ads_list = ListPage.as_view(model=Ad)
books = ListPage.as_view(model=Book)
contests_list = ListPage.as_view(model=Contest)
directions = ListPage.as_view(model=Direction)
quiz_list = ListPage.as_view(model=Quiz)
newbies = ListPage.as_view(model=Newbie)
empltraining_list = ListPage.as_view(model=EmplTraining)
plans = ListPage.as_view(model=Plan,
                        template_name='plans.html')
questions = ListPage.as_view(model=Question, template_name='questions.html')

class CompanyLifePage(IndexPage):
    u''' Жизнь компании '''
    template_name = 'company_life.html'

    def get_context_data(self, **kwargs):
        ctx = super(CompanyLifePage, self).get_context_data(**kwargs)
        ctx['quiz_list'] = slice_list(Quiz.objects.all())
        ctx['image_galleries'] = slice_list(Gallery.objects.filter(glr_type=0))
        ctx['video_galleries'] = slice_list(Gallery.objects.filter(glr_type=1))
        ctx['questions'] = slice_list(Question.objects.all())
        return ctx
company_life = CompanyLifePage.as_view()


class InfoPage(IndexPage):
    u''' Справочная информация '''
    template_name = 'info.html'

    def get_context_data(self, **kwargs):
        ctx = super(self.__class__, self).get_context_data(**kwargs)
        subsidairies = Subsidairy.objects.filter(phonebook__isnull=False
                                        ).values_list('id', flat=True)
        ctx['phones'] = slice_list(
                            Subsidairy.objects.filter(id__in=subsidairies),
                            5
        )
        ctx['docs'] = slice_list(DocTemplate.objects.all(), 4)
        ctx['galleries'] = slice_list(Gallery.objects.all())
        ctx['plans']=slice_list(Plan.objects.all(), 15)
        return ctx
info = InfoPage.as_view()


class StaffPage(IndexPage):
    u''' Сотрудники '''
    template_name = 'staff.html'

    def get_context_data(self, **kwargs):
        ctx = super(StaffPage, self).get_context_data(**kwargs)
        ctx['company_structure'] = slice_list(
            CompanyStructure.objects.filter(parent__isnull=True),
            5
        )
        st = SystemTunes.objects.all().last()
        if st:
            ctx['system_tunes'] = st
        ctx['new_users'] = slice_list(User.objects.new_users())
        #ctx['on_vacations'] = slice_list(Vacation.objects.on_vacation_users())
        ctx['birth_date'] = slice_list(BirthDate.objects.birth_users())
        ctx['empltraining'] = slice_list(EmplTraining.objects.all())
        return ctx
staff = StaffPage.as_view()


class DetailPage(ContextMixin, DetailView):
    template_name = 'object_detail.html'
    context_object_name = 'detail_page'

    def get_context_data(self, **kwargs):
        ctx = super(DetailPage, self).get_context_data(**kwargs)
        ctx['reverse_url_name'] = ctx.get('detail_page'
                                    ).__class__._meta.verbose_name_plural.lower()
        return ctx

ads = DetailPage.as_view(model=Ad)
book = DetailPage.as_view(model=Book)
calendar_event = DetailPage.as_view(model=Calendar)
contests = DetailPage.as_view(model=Contest)
direction = DetailPage.as_view(model=Direction)
news = DetailPage.as_view(model=News)
newbie = DetailPage.as_view(model=Newbie)
empltraining = DetailPage.as_view(model=EmplTraining)
question = DetailPage.as_view(model=Question)

class QuizDetail(DetailPage):
    model=Quiz
    template_name='quiz.html'

    def get_context_data(self, **kwargs):
        ctx = super(QuizDetail, self).get_context_data(**kwargs)
        ctx['can_vote'] = self.can_vote()
        ctx['results'] = self.quiz_results()
        return ctx

    def can_vote(self):
        self.quv_set = self.get_object().quizuservote_set
        vote_users_pk = self.quv_set.values_list('user', flat=True)
        return self.request.user.pk not in vote_users_pk

    def quiz_results(self):
        if not self.can_vote():
            quv_set = self.get_object().quizuservote_set.order_by('choice')
            count = quv_set.aggregate(Count('user'))
            by_ch =  quv_set.values('choice__title'
                            ).annotate(num_users=Count('user'))
            return count, by_ch

    def post(self, request, *args, **kwargs):
        if self.can_vote() and self.request.user.is_authenticated():
            postdata = self.request.POST
            QuizUserVote.objects.create(quiz=self.get_object(),
                                        user = self.request.user,
                                        choice_id = postdata.get('quiz_choice'))
            return redirect(self.get_object().get_absolute_url())
        return self.http_method_not_allowed(request)  
quiz = QuizDetail.as_view()

class BirthDetail(DetailPage):
    model=BirthDate
    template_name = 'birth_detail.html'
    def get_context_data(self, **kwargs):
        ctx = super(BirthDetail, self).get_context_data(**kwargs)
        ctx['wish_form'] = WishForm()
        return ctx

    def post(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            postdata = self.request.POST
            form = WishForm(postdata)
            if form.is_valid():
                wish = form.save(commit=False)
                wish.user = request.user
                wish.save()
                self.get_object().wishes.add(wish)
        return redirect(self.get_object().get_absolute_url())
birth = BirthDetail.as_view()


class QuestionDetail(DetailPage):
    model=Question
    template_name = 'question_detail.html'
    def get_context_data(self, **kwargs):
        ctx = super(QuestionDetail, self).get_context_data(**kwargs)
        ctx['comment_form'] = CommentForm()
        return ctx

    def post(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            postdata = self.request.POST
            form = CommentForm(postdata)
            if form.is_valid():
                comment = form.save(commit=False)
                comment.user = request.user
                comment.save()
                self.get_object().comments.add(comment)
        return redirect(self.get_object().get_absolute_url())
question = QuestionDetail.as_view()


class AdCreate(CreateView):
    model = Ad
    fields=('title', 'content')
    template_name = 'create_form.html'

    def form_valid(self, form):
        if self.request.user.is_authenticated():
                ad = form.save(commit=False)
                ad.user = self.request.user
                ad.save()
                return super(AdCreate, self).form_valid(form)
        return self.form_invalid(self, form)
create_ad = AdCreate.as_view()


class QuestionCreate(CreateView):
    model = Question
    fields=('title', 'content')
    template_name = 'create_qform.html'

    def form_valid(self, form):
        if self.request.user.is_authenticated():
                question = form.save(commit=False)
                question.user = self.request.user
                question.save()
                return super(QuestionCreate, self).form_valid(form)
        return self.form_invalid(self, form)
create_question = QuestionCreate.as_view()