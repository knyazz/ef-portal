#coding:  utf-8
from django import forms
from django.contrib import admin
from django.core.exceptions import ValidationError
from django.db import models

#from django_select2.widgets import Select2MultipleWidget
from mptt.fields import TreeForeignKey
from mptt.admin import MPTTModelAdmin
from suit.admin import SortableModelAdmin
from suit.widgets import LinkedSelect
from suit.widgets import SuitDateWidget

from .models import Ad, Book, Calendar, CompanyStructure, Contest
from .models import Direction, DirectionType#, Question, Comments
from .models import News, NewsSliderItem, Vacation, Quiz, Choice#, QuizUserVote
from .models import PlanType, Plan, BirthDate, SystemTunes, Newbie, EmplTraining


class AdminBaseModel(admin.ModelAdmin):
    formfield_overrides = {
        models.DateField: {'widget': SuitDateWidget},
        models.ForeignKey: {'widget': LinkedSelect},
        models.OneToOneField: {'widget': LinkedSelect},
        TreeForeignKey: {'widget': LinkedSelect},
        #models.ManyToManyField: {'widget': Select2MultipleWidget(select2_options={'width': 'resolve'})}
    }
    class Media:
        #css = {'all': ('css/jquery-ui-1.10.4.min.css',)}
        js = (
                #'js/jquery-ui-1.10.4.min.js',
                'js/admin.js',
        )

admin.site.register(SystemTunes, AdminBaseModel)

class DefaultPage(AdminBaseModel):
    exclude = ['user',]
    def save_model(self, request, obj, form, change):
        obj.user = request.user
        super(DefaultPage, self).save_model(request, obj, form, change)

class PageAdmin(DefaultPage):
    fieldsets = (
        (None, {'fields': ('title',),}),
        (None, {
            'classes': ('full-width',),
            'fields': ('content',)
        }),
        (None, {'fields': ('is_active', 'is_publish'),}),
    )

class DirectionPageAdmin(PageAdmin):
    def __init__(self, *args, **kwargs):
        super(DirectionPageAdmin, self).__init__(*args, **kwargs)
        self.fieldsets+= (
        (None, {
            'fields': ( 'department', 'dirtype')
        }),)
admin.site.register(Direction, DirectionPageAdmin)


class CalendarPageAdmin(PageAdmin):
    fieldsets = (
        (None, {'fields': ('title',),}),
        (None, {
            'classes': ('full-width',),
            'fields': ('content',)
        }),
        (None, {'fields': ( 'start_date',
                            'end_date',
                            'is_active',
                            'is_publish'),
        }),
    )

admin.site.register(Ad, PageAdmin)
admin.site.register(Book, PageAdmin)
admin.site.register(Calendar, CalendarPageAdmin)
admin.site.register(Contest, PageAdmin)
admin.site.register(DirectionType)
admin.site.register(News, PageAdmin)
admin.site.register(PlanType)
admin.site.register(Plan)
admin.site.register(Vacation)
admin.site.register(BirthDate, AdminBaseModel)
admin.site.register(Newbie, AdminBaseModel)
admin.site.register(EmplTraining, AdminBaseModel)

class CompanyStructureAdmin(MPTTModelAdmin, SortableModelAdmin, DefaultPage):
    mptt_level_indent = 20
    list_display = ('name',)
    sortable = 'order'
admin.site.register(CompanyStructure, CompanyStructureAdmin)


admin.site.register(Choice)


class ChoicesFormsetBase(forms.models.BaseInlineFormSet):
    def clean(self):
        super(ChoicesFormsetBase, self).clean()

        initial_num = len(filter(lambda f: not self._should_delete_form(f), self.initial_forms))
        extra_num = len(filter(lambda f: f.has_changed() and not self._should_delete_form(f), self.extra_forms))
        if initial_num + extra_num < 2:
            raise ValidationError(u"Должно быть как минимум два варианта ответа")

ChoiceFormset = forms.models.inlineformset_factory(Quiz, Quiz.choices.through, formset=ChoicesFormsetBase)

class ChoiceInline(admin.TabularInline):
    model = Quiz.choices.through
    verbose_name=Choice._meta.verbose_name
    verbose_name_plural = Choice._meta.verbose_name_plural
    formset = ChoiceFormset
    min_num = 2
    extra = 0
    formfield_overrides = {
        models.DateField: {'widget': SuitDateWidget},
        models.ForeignKey: {'widget': LinkedSelect},
        models.OneToOneField: {'widget': LinkedSelect},
        TreeForeignKey: {'widget': LinkedSelect},
    }


class QuizAdmin(PageAdmin):
    inlines = (ChoiceInline,)
admin.site.register(Quiz, QuizAdmin)
#admin.site.register(QuizUserVote)


class NewsSliderItemAdmin(SortableModelAdmin, DefaultPage):
    sortable = 'order'
    fieldsets = (
        (None, {'fields': ('title',),}),
        (None, {
            'classes': ('full-width',),
            'fields': ('content',)
        }),
        (None, {'fields': ( 'news', 'image', 'is_active', 'is_publish'),}),
    )
admin.site.register(NewsSliderItem, NewsSliderItemAdmin)