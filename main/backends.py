#coding: utf-8
from django_auth_ldap.backend import LDAPBackend, _LDAPUser


class MYLDAPBackend(LDAPBackend):
    def put_users_in_db(self, username):
        ldap_user = _LDAPUser(self, username=username.strip())
        self.get_or_create_user(username, ldap_user)
        return self.populate_user(username=username)
