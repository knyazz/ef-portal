#coding: utf-8

def slice_list(qs=None, count=3):
    if qs and qs.count()>count:
        return qs[:count]
    return qs