﻿$(document).ready(function(){
	ang('#topprofileblock .avatarinput').change(function(){
		$('#topprofileblock').submit();
		});
    ang('.accordiontitle, .accordionlink').click(function(){
        $(this).siblings().slideToggle();
        $(this).parent().toggleClass('closed');
        });
});

function changeslide(slideNum,slideNav,slideList){
	slideNav.find('a').removeClass('active');
	slideNav.find('li').eq(slideNum).find('a').addClass('active');
	slideList.find('li').eq(slideNum).fadeIn();
	slideList.find('li').not(slideList.find('li').eq(slideNum)).fadeOut(0);
	};
	
function nextSlide(slideNav){
	var nextNum = slideNav.find('.active').parent().index()+1;
	var maxNum = slideNav.find('li').size();
	if(nextNum >= maxNum){
		nextNum = 0;
		}
	slideNav.find('a').eq(nextNum).trigger('click');
	}


function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});