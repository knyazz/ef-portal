$(function() {
  if (typeof CKEDITOR !== 'undefined') {
    CKEDITOR.config.disableNativeSpellChecker = false;
    //CKEDITOR.config.contentsCss = '/static/css/style.css'
    CKEDITOR.config.forcePasteAsPlainText = true;

    CKEDITOR.on('dialogDefinition', function (ev) {
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;
        dialogDefinition.minHeight =100;
        dialogDefinition.minWidth = 400;
        var editor = ev.editor;

        dialogDefinition.removeContents('Link');
        dialogDefinition.removeContents('advanced');

        if (dialogName == 'image') {
            var infoTab = dialogDefinition.getContents('info');
            dialogDefinition.onOk = function (e) {
                console.log(dialogDefinition.dialog.getValueOf("info", "txtHeight"));
                var imageSrcUrl = e.sender.originalElement.$.src;
                var width = e.sender.originalElement.$.width;
                var height = e.sender.originalElement.$.height;
                var txtHeight = dialogDefinition.dialog.getValueOf("info", "txtHeight");
                var txtWidth = dialogDefinition.dialog.getValueOf("info", "txtWidth");
                if (txtHeight) {
                  height = txtHeight;
                };
                if (txtWidth) {
                  width = txtWidth;
                };
                imageSrcUrl = imageSrcUrl.replace(/^.*\/\/[^\/]+/, '');
                var imgHtml = CKEDITOR.dom.element.createFromHtml('<a class=\"fancybox\" href=\"' + imageSrcUrl + '\"><img src=' + imageSrcUrl + ' width=\"' + width + '\" height=\"' + height + '\" alt=\"\" /></a>');
                editor.insertElement(imgHtml);
            };

            infoTab.remove('txtHSpace');
            infoTab.remove('txtVSpace');
            infoTab.remove('txtBorder');
            //infoTab.remove('txtHeight');
            //infoTab.remove('txtWidth');
            infoTab.remove('cmbAlign');
            infoTab.remove('ratioLock');

            infoTab.get('htmlPreview').style = 'display: none;'
        }
    });
  }
});

$(function(){
  function set_image() {
      $('div.control-group.form-row.field-image').show();
      $('div.control-group.form-row.field-video').hide();
      $('div.control-group.form-row.field-fl').hide();
      $('input[name=fl]').val("");
      $('input[name=fl]').attr("required",false);
      $('input[name=video]').val("");
      $('input[name=video]').attr("required",false);
      $('input[name=image]').attr("required","required");
  }
  set_image();
  $("#id_fl_type").on("change", function() {
    if ($("#id_fl_type").val() == 0) {
      set_image();
    }
    else if ($("#id_fl_type").val() == 1) {
      $('div.control-group.form-row.field-image').hide();
      $('div.control-group.form-row.field-video').show();
      $('div.control-group.form-row.field-fl').hide();
      $('input[name=fl]').val("");
      $('input[name=fl]').attr("required",false);
      $('input[name=image]').val("");
      $('input[name=image]').attr("required",false);
      $('input[name=video]').attr("required","required");
    }
    else if ($("#id_fl_type").val() == 2) {
      $('div.control-group.form-row.field-image').hide();
      $('div.control-group.form-row.field-video').hide();
      $('div.control-group.form-row.field-fl').show();
      $('input[name=video]').val("");
      $('input[name=video]').attr("required",false);
      $('input[name=image]').val("");
      $('input[name=image]').attr("required",false);
      $('input[name=fl]').attr("required","required");
    }
  });
});