$(function(){
  $( document ).on( "change", function( event ) {
    var fl_type_obj  = $( event.target ).closest( ".control-group.form-row.field-fl_type" )
    var image = fl_type_obj.next();
    var fl = image.next();
    var video = fl.next();
    var fl_type = fl_type_obj.find('select').val()
    if (fl_type == 0) {
      image.show();
      video.hide();
      fl.hide();
    }
    else if (fl_type == 1) {
      image.hide();
      fl.hide();
      video.show();
    }
    else if (fl_type == 2) {
      image.hide();
      video.hide();
      fl.show();
    }
  });
});

$(function(){
  function set_media(val) {
    var fl_type = val.find('.control-group.form-row.field-fl_type'
                    ).find('select').val();

    if (fl_type == 0) {
      val.find('.control-group.form-row.field-image').show();
      val.find('.control-group.form-row.field-video').hide();
      val.find('.control-group.form-row.field-fl').hide();
    }
    else if (fl_type == 1) {
      val.find('.control-group.form-row.field-image').hide();
      val.find('.control-group.form-row.field-fl').hide();
      val.find('.control-group.form-row.field-video').show();
    }
    else if (fl_type == 2) {
      val.find('.control-group.form-row.field-image').hide();
      val.find('.control-group.form-row.field-fl').show();
      val.find('.control-group.form-row.field-video').hide();
    }
  }

  $('div.tab-content.tab-content-main').find('.inline-related').each(function( index ) {
    set_media($( this ).children('fieldset.module.aligned.first'));
  })
});