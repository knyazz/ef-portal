#coding: utf-8
from django import forms

from ckeditor.widgets import CKEditorWidget

from .models import Ad, Wish, Comment


class WishForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())
    class Meta:
        model = Wish
        fields = 'content',


class CommentForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())
    class Meta:
        model = Comment
        fields = 'content',


class AdForm(forms.ModelForm):
    class Meta:
        model = Ad
        fields = 'title', 'content'