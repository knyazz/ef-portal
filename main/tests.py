# -*- coding: utf-8 -*-
from django.core.management import call_command
from django.core.urlresolvers import reverse
from django.test.client import Client

from django_nose.testcases import FastFixtureTestCase


class BaseTest(FastFixtureTestCase):
    fixtures = (
                'finter/fixtures/init.json',
                #'kvc/fixtures/users.json',
                #'kvc/fixtures/testcase.json',
            )
    def setUp(self):
        self.test_client = Client()

    @classmethod
    def _fixture_teardown(cls):
        fixtures = getattr(cls, 'fixtures', None)
        _fb_should_teardown_fixtures = getattr(cls, '_fb_should_teardown_fixtures', True)
        # _fixture_teardown needs help with constraints.
        if fixtures and _fb_should_teardown_fixtures:
            call_command('flush', interactive=False, verbosity=1)
        return super(BaseTest, cls)._fixture_teardown()

    def get_simple_page(self, url, kw=None):
        kw = kw or {}
        response = self.test_client.get(reverse(url, kwargs=kw))
        self.assertEqual(response.status_code, 200)
        return response

    def get_simple_pages(self, url_lst):
        for url in url_lst:
            self.get_simple_page(url)

    def get_list_and_detail_pages(self, url_dict):
        for obj_list_url, obj_detail_url in url_dict.items():
            # get obj_list page
            response = self.get_simple_page(obj_list_url)
            print obj_detail_url
            ad_pk = response.context[0].get('object_list')[0].pk
            # get obj detail page
            self.get_simple_page(obj_detail_url, kw=dict(pk=ad_pk))     


class MainfuncTest(BaseTest): 
    def base_simple_tests(self):
        '''
            test base functionality
        '''
        url_lst = ( 'main:index',
                    'main:plans',
                    'main:staff',
                    'main:info',
                    'main:company_life',)
        self.get_simple_pages(url_lst)

        url_dict = {
            'main:ads_list': 'main:ads',
            'main:books': 'main:book',
            'accounts:birthday_users': 'main:birth',
            'main:contests_list': 'main:contests',
            'main:directions': 'main:direction',
            'main:quiz_list': 'main:quiz',
            'main:news_list': 'main:news',
            #'main:newbies': 'main:newbie.',
        }
        self.get_list_and_detail_pages(url_dict)