# coding: utf-8
import datetime

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models

from ckeditor.fields import RichTextField
from easy_thumbnails.fields import ThumbnailerImageField
from easy_thumbnails.files import get_thumbnailer
from filer.fields.image import FilerImageField
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel

from .managers import VacationManager, BirthManager


class CompanyStructure(MPTTModel):
    u''' Оргструктура '''
    name = models.CharField(max_length=255)
    parent = TreeForeignKey('self', null=True, blank=True,
                            verbose_name=u'Родитель',
                            related_name='children')
    order = models.PositiveIntegerField()
    content = RichTextField(blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True,
                            verbose_name=u'Пользователь')

    class MPTTMeta:
        order_insertion_by = ('order',)

    def save(self, *args, **kwargs):
        super(CompanyStructure, self).save(*args, **kwargs)
        CompanyStructure.objects.rebuild()

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = verbose_name = u'Структура компании'

class SystemTunes(models.Model):
    cs_image = FilerImageField(verbose_name=u'Изображение оргструктуры')
    class Meta:
        verbose_name= u'Системные настройки'
        verbose_name_plural = verbose_name
    @property
    def cs_image_url(self):
        if self.cs_image:
            opts = dict(size=(172,258))
            thumbnail = get_thumbnailer(self.cs_image).get_thumbnail(opts)
            return thumbnail.url


class DefaultModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                            verbose_name=u'Пользователь')

    class Meta:
        ordering = ('-created',)
        abstract = True


class Vacation(models.Model):
    objects = VacationManager()
    start_date = models.DateField(default=datetime.date.today())
    end_date = models.DateField(default=datetime.date.today())
    is_active = models.BooleanField(default=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                            verbose_name=u'Пользователь')
    class Meta:
        verbose_name_plural = verbose_name = u'Отпуск'


class Wish(models.Model):
    content = RichTextField(max_length=1024)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=u'Пользователь')
    class Meta:
        verbose_name=u'Пожелание'
        verbose_name_plural = u'Пожелания'


class BirthDate(models.Model):
    objects = BirthManager()
    title = models.CharField(u'Заголовок', max_length=255)
    content = RichTextField(blank=True)
    birth_date = models.DateField(default=datetime.date.today())
    is_active = models.BooleanField(default=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True,
                            verbose_name=u'Пользователь')
    wishes = models.ManyToManyField(Wish, verbose_name=Wish._meta.verbose_name_plural,
                                    null=True, blank=True)
    __unicode__ = lambda self: self.title
    class Meta:
        ordering = '-birth_date', 'user'
        verbose_name = u'День рождения'
        verbose_name_plural = u'Дни рождения'

    def get_absolute_url(self):
        return reverse('main:birth', kwargs=dict(pk=self.pk))

    @classmethod
    def list_url(cls):
        return reverse('accounts:birthday_users')


class Newbie(models.Model):
    title = models.CharField(u'Заголовок', max_length=255)
    content = RichTextField(blank=True)    
    __unicode__ = lambda self: self.title
    class Meta:
        verbose_name = u'Новичок'
        verbose_name_plural = u'Новички'

    def get_absolute_url(self):
        return reverse('main:newbie', kwargs=dict(pk=self.pk))


class EmplTraining(models.Model):
    title = models.CharField(u'Заголовок', max_length=255)
    content = RichTextField(blank=True)    
    __unicode__ = lambda self: self.title
    class Meta:
        ordering = '-pk',
        verbose_name = u'Обучение сотрудника'
        verbose_name_plural = u'Обучение сотрудников'

    def get_absolute_url(self):
        return reverse('main:empltraining', kwargs=dict(pk=self.pk))


class TitlePage(models.Model):
    title = models.CharField(u'Заголовок', max_length=255)
    is_active = models.BooleanField(u'Актуально', default=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.title + u' ' + unicode(self.created)

    class Meta:
        abstract = True

class AbstractPage(TitlePage):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                            verbose_name=u'Пользователь')
    content = RichTextField(blank=True)
    is_publish = models.BooleanField(u'Опубликовано', default=True)
    class Meta:
        ordering = ('-created','title','user')
        abstract = True


class News(AbstractPage):
    u''' Новости портала '''
    class Meta:
        ordering = ('-created','title','user')
        verbose_name=u'Новость'
        verbose_name_plural = u'Новости'

    def get_absolute_url(self):
        return reverse('main:news', kwargs=dict(pk=self.pk))

    @classmethod
    def list_url(cls):
        return reverse('main:news_list')


class Ad(AbstractPage):
    u''' Доска Объявлений портала '''
    class Meta:
        ordering=('-created',)
        verbose_name=u'Объявление сотрудника'
        verbose_name_plural = u'Объявления сотрудников'

    def get_absolute_url(self):
        return reverse('main:ads', kwargs=dict(pk=self.pk))

    @classmethod
    def list_url(cls):
        return reverse('main:ads_list')


class Contest(AbstractPage):
    u'''конкурсы '''
    class Meta:
        ordering=('-created',)
        verbose_name=u'Конкурс'
        verbose_name_plural = u'Конкурсы'

    def get_absolute_url(self):
        return reverse('main:contests', kwargs=dict(pk=self.pk))

    @classmethod
    def list_url(cls):
        return reverse('main:contests_list')


class Choice(models.Model):
    title = models.CharField(u'Название выбора', max_length=255)
    __unicode__ = lambda self: self.title
    class Meta:
        verbose_name=u'Элемент выбора В Опросе'
        verbose_name_plural = u'Элементы выбора В Опросе'

class Quiz(AbstractPage):
    choices = models.ManyToManyField(Choice, verbose_name=u'Варианты')

    def get_absolute_url(self):
        return reverse('main:quiz', kwargs=dict(pk=self.pk))

    @classmethod
    def list_url(cls):
        return reverse('main:quiz_list')

    class Meta:
        ordering = ('-created',)
        verbose_name=u'Опрос'
        verbose_name_plural = u'Опросы'


class QuizUserVote(models.Model):
    quiz = models.ForeignKey(Quiz, verbose_name=Quiz._meta.verbose_name)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                            verbose_name=u'Пользователь')
    choice = models.ForeignKey(Choice, verbose_name=Choice._meta.verbose_name)

    def save(self, *args, **kwargs):
        if not self.pk:
            checks = self.__class__.objects.filter( quiz =self.quiz,
                                                    user=self.user,
                                                    choice=self.choice
                                        ).exists()
            if checks:
                return
        return super(QuizUserVote,self).save(*args, **kwargs)


class Calendar(AbstractPage):
    u''' календарь событий '''
    start_date = models.DateField(default=datetime.date.today())
    end_date = models.DateField(default=datetime.date.today())

    class Meta:
        ordering = ('start_date',)
        verbose_name=u'Событие календаря'
        verbose_name_plural = u'Календарь событий'

    def get_absolute_url(self):
        return reverse('main:calendar_event', kwargs=dict(pk=self.pk))

    @classmethod
    def list_url(cls):
        return reverse('main:calendar')


class NewsSliderItem(AbstractPage):
    image = ThumbnailerImageField(u'Изображение',
                                upload_to = 'uploads/',
                                resize_source=dict(size=(902, 596),),
    )
    news = models.OneToOneField(News, verbose_name=News._meta.verbose_name)
    order = models.PositiveIntegerField(u'Сортировка', default=0)

    class Meta:
        ordering = ('order',)
        verbose_name= u'Элемент слайдера'
        verbose_name_plural = u'Элементы слайдера'


class Book(AbstractPage):
    u''' книги портала '''
    class Meta:
        ordering = ('-created','title','user')
        verbose_name=u'Книга'
        verbose_name_plural = u'Книги'

    def get_absolute_url(self):
        return reverse('main:book', kwargs=dict(pk=self.pk))

    @classmethod
    def list_url(cls):
        return reverse('main:books')


class DirectionType(models.Model):
    title = models.CharField(u'Заголовок', max_length=255)
    def __unicode__(self):
        return self.title
    class Meta:
        verbose_name=u'Тип дирекции'
        verbose_name_plural=u'Типы дирекций'


class Direction(AbstractPage):
    u''' книги портала '''
    department = models.ForeignKey(CompanyStructure,
                            verbose_name=CompanyStructure._meta.verbose_name)
    dirtype = models.ForeignKey(DirectionType,
                                verbose_name=DirectionType._meta.verbose_name)
    class Meta:
        ordering = ('-created','title','user')
        verbose_name=u'Дирекция'
        verbose_name_plural = u'Дирекции'

    def get_absolute_url(self):
        return reverse('main:direction', kwargs=dict(pk=self.pk))

    @classmethod
    def list_url(cls):
        return reverse('main:directions')


class PlanType(models.Model):
    title = models.CharField(u'Заголовок', max_length=255)
    def __unicode__(self):
        return self.title
    class Meta:
        verbose_name=u'Категиория плана'
        verbose_name_plural=u'Категории планов'


class Plan(models.Model):
    title = models.CharField(u'Заголовок', max_length=255)
    planfile = FilerImageField()
    category = models.ForeignKey(PlanType,
                                verbose_name=PlanType._meta.verbose_name)
    def __unicode__(self):
        return self.title
    class Meta:
        verbose_name=u'План комплекса'
        verbose_name_plural=u'Планы комплекса'

    def get_absolute_url(self):
        return reverse('main:plan', kwargs=dict(pk=self.pk))

    @classmethod
    def list_url(cls):
        return reverse('main:plans')


class Comment(models.Model):
    content = RichTextField(max_length=1024)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=u'Пользователь')
    class Meta:
        verbose_name=u'Комментарий'
        verbose_name_plural = u'Комментарии'


class Question(AbstractPage):
    u''' вопросы портала '''
    comments = models.ManyToManyField(Comment, null=True, blank=True,
                                    verbose_name=Comment._meta.verbose_name_plural)
    class Meta:
        ordering = ('-created','title','user')
        verbose_name=u'Вопрос'
        verbose_name_plural = u'Вопросы'

    def get_absolute_url(self):
        return reverse('main:question', kwargs=dict(pk=self.pk))

    @classmethod
    def list_url(cls):
        return reverse('main:questions')