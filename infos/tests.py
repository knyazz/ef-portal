# -*- coding: utf-8 -*-
from main.tests import BaseTest


class InfosTest(BaseTest):
    def filestorage_simple_tests(self):
        '''
            test infos functionality
        '''
        url_lst = ( 'infos:files',
                    'infos:phones')
        self.get_simple_pages(url_lst)

        self.get_simple_page('infos:subsidairy', kw=dict(pk=1))