#coding: utf-8
from django.db.models import Q

from main.views import ListPage, DetailPage

from .models import DocTemplate, Subsidairy


class FileList(ListPage):
    model=DocTemplate
    template_name='docs.html'

    def get_queryset(self):
        qs = super(FileList, self).get_queryset()
        #TODO: refact this
        q = Q()
        if self.request.GET:
            body = self.request.GET
            if body.get('title'):
                fld = body.get('title')
                q&= Q(title__icontains=fld)
        return qs.filter(q)
files = FileList.as_view()


subsidairy = DetailPage.as_view(model=Subsidairy,
                        template_name='subsidairy.html')


class PhoneList(ListPage):
    model=Subsidairy
    queryset=Subsidairy.objects.filter(phonebook__isnull=False)
    template_name='phonebook.html'

    def get_queryset(self):
        qs = super(PhoneList, self).get_queryset()
        #TODO: refact this
        q = Q()
        if self.request.GET:
            body = self.request.GET
            if body.get('fio'):
                fld = body.get('fio')
                q&= Q(title__icontains=fld)
        return qs.filter(q)
phones = PhoneList.as_view()