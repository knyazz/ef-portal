#coding: utf-8

FILETYPES = (
    ('xls', 'XLS'),
    ('pdf', 'PDF'),
    ('doc', 'DOC'),
    ('docx', 'DOCX')
)