from django.conf.urls import patterns, url

urlpatterns = patterns('infos.views',
    url(r'^files/$', 'files', name='files'),
    url(r'^phones/$', 'phones', name='phones'),
    url(r'^subsidairy/(?P<pk>\d+)/$', 'subsidairy', name='subsidairy'),
)