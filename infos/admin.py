from django.contrib import admin

from main.admin import DefaultPage

from .models import DocTemplate, PhoneBook, Subsidairy

admin.site.register(DocTemplate, DefaultPage)
admin.site.register(PhoneBook, DefaultPage)
admin.site.register(Subsidairy, DefaultPage)
