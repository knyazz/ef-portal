#coding: utf-8
from django.db import models
from django.core.urlresolvers import reverse

from main.models import DefaultModel, TitlePage

from .choices import FILETYPES


class Subsidairy(TitlePage):
    u''' Дочерняя компания '''
    __unicode__ = lambda self: self.title
    class Meta:
        verbose_name=u'Дочерняя компания'
        verbose_name_plural = u'Дочернии компании'

    def get_absolute_url(self):
        return reverse('infos:subsidairy', kwargs=dict(pk=self.pk))


class DocTemplate(DefaultModel):
    title = models.CharField(max_length=255)
    template = models.FileField(upload_to='docs/')
    file_type = models.CharField(max_length=32, choices=FILETYPES)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name=u'шаблон документа'
        verbose_name_plural = u'шаблоны документов'


class PhoneBook(DefaultModel):
    title = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)
    subsidairy = models.ForeignKey(Subsidairy, blank=True, null=True,
                                verbose_name=Subsidairy._meta.verbose_name)
    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name=u'Телефонный справочник'
        verbose_name_plural = verbose_name