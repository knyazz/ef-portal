#coding: utf-8
from main.views import DetailPage, ListPage

from .models import Gallery

class FilestorageBaseList(ListPage):
    model = Gallery
    template_name='gallery_list.html'
gallery_list = FilestorageBaseList.as_view()
image_gallery_list = FilestorageBaseList.as_view(
    queryset=Gallery.objects.filter(glr_type=0)
)
video_gallery_list = FilestorageBaseList.as_view(
    queryset=Gallery.objects.filter(glr_type=1)
)
docs_gallery_list = FilestorageBaseList.as_view(
    queryset=Gallery.objects.filter(glr_type=2)
)


class FilestorageBaseDetail(DetailPage):
    model = Gallery
    template_name='gallery.html'
gallery = FilestorageBaseDetail.as_view()
image_gallery = FilestorageBaseDetail.as_view(
                                    queryset=Gallery.objects.filter(glr_type=0),
)
video_gallery = FilestorageBaseDetail.as_view(
                                    queryset=Gallery.objects.filter(glr_type=0),
)
docs_gallery = FilestorageBaseDetail.as_view( 
                                    queryset=Gallery.objects.filter(glr_type=0),
)