# -*- coding: utf-8 -*-
from main.tests import BaseTest


class FilestorageTest(BaseTest):
    def filestorage_simple_tests(self):
        '''
            test filestorage functionality
        '''
        url_lst = ( 'filestorage:video_gallery_list',
                    'filestorage:docs_gallery_list')
        self.get_simple_pages(url_lst)

        url_dict = {
            'filestorage:gallery_list': 'filestorage:gallery',
            'filestorage:image_gallery_list': 'filestorage:image_gallery',
            #'filestorage:video_gallery_list': 'filestorage:video_gallery',
            #'filestorage:docs_gallery_list': 'filestorage:docs_gallery',
        }
        self.get_list_and_detail_pages(url_dict) 