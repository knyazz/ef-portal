# coding: utf-8
from django.db import models
from django.core.urlresolvers import reverse

from easy_thumbnails.files import get_thumbnailer
from embed_video.fields import EmbedVideoField
from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField

from main.models import AbstractPage, TitlePage

from .choices import MEDIA_FILETYPES, GALLERY_TYPES


class Gallery(AbstractPage):
    order = models.PositiveIntegerField(u'Сортировка')
    glr_type = models.PositiveSmallIntegerField(u'Тип галерии', default=0,
                                                choices=GALLERY_TYPES)
    class Meta:
        ordering = 'order', '-created'
        verbose_name=u'Медиа банк'
        verbose_name_plural = u'Медиа банк'

    def get_absolute_url(self):
        return reverse('filestorage:gallery', kwargs=dict(pk=self.pk))


class Mediafile(TitlePage):
    order = models.PositiveIntegerField(u'Сортировка', default=0)
    fl_type = models.PositiveSmallIntegerField(u'Тип файла', default=0,
                                                choices=MEDIA_FILETYPES)
    image = FilerImageField(verbose_name=u'Изображение',
                            related_name='mediaimages',
                            null=True, blank=True)
    fl = FilerFileField(verbose_name=u'Файл (pdf)', null=True, blank=True,)
    video = EmbedVideoField(blank=True,
                            help_text=u'например: http://www.youtube.ru/video')
    gallery = models.ForeignKey(Gallery, null=True, blank=True,
                                verbose_name=Gallery._meta.verbose_name)

    class Meta:
        ordering = 'order', 'pk'
        verbose_name=u'Медиа файл'
        verbose_name_plural = u'Медиа файлы'

    def get_absolute_url(self):
        return reverse('filestorage:mediafile', kwargs=dict(pk=self.pk))

    is_image = property(lambda self: self.image and self.fl_type == 0)
    is_video = property(lambda self: self.video and self.fl_type == 1)
    is_file = property(lambda self: self.fl and self.fl_type == 2)

    @property
    def small_image(self):
        if self.is_image:
            opts = dict(size=(150,100), crop=True)
            try:
                thumbnail = get_thumbnailer(self.image).get_thumbnail(opts)
                return thumbnail.url
            except: pass