#coding: utf-8
from django.contrib import admin

from suit.admin import SortableModelAdmin, SortableStackedInline

from main.admin import PageAdmin, DefaultPage

from .models import Mediafile, Gallery


class MediafileAdmin(SortableModelAdmin, DefaultPage):
    sortable = 'order'
    class Media:
        js = (
                'js/admin.js',
                'js/jquery-ui-1.10.4.min.js'
        )
admin.site.register(Mediafile, MediafileAdmin)


class MediafilesInline(SortableStackedInline):
    model = Mediafile
    sortable = 'order'
    extra=0


class GalleryAdmin(SortableModelAdmin, PageAdmin):
    inlines = (MediafilesInline,)
    sortable = 'order'
    class Media:
        js = (
                'js/admin-mediaflsets.js',
        )
    def __init__(self, *args, **kwargs):
        super(GalleryAdmin, self).__init__(*args, **kwargs)
        self.fieldsets+= (
        (None, {
            'fields': ( 'glr_type',)
        }),)
admin.site.register(Gallery, GalleryAdmin)