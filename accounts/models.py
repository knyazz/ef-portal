# coding: utf-8
import datetime

from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from django.core.urlresolvers import reverse

from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.fields import ThumbnailerImageField


class UserQuerySet(models.query.QuerySet):
    start_date = datetime.date.today() - datetime.timedelta(days=7)
    end_date = datetime.date.today() + datetime.timedelta(days=7)

    def new_users(self):
        return self#.filter(date_joined__lte=datetime.date.today())
        #return self.filter(date_joined__gte=self.start_date)

    def birthday_users(self):
        return self.filter( date_of_birth__isnull=False,
                            date_of_birth__gte=self.start_date,
                            date_of_birth__lte=self.end_date,)

    def with_phones(self):
        return self.filter(phone__isnull=False
                  ).exclude(phone__exact='')


class UserManager(UserManager):
    def get_queryset(self):
        return UserQuerySet(self.model, using=self._db)

    def new_users(self):
        return self.get_queryset().new_users()

    def birthday_users(self):
        return self.get_queryset().birthday_users()

    def with_phones(self):
        return self.get_queryset().with_phones()

class User(AbstractUser):
    objects = UserManager()
    avatar = ThumbnailerImageField( u'Аватар',
                                    upload_to = 'uploads/avatars/',
                                    resize_source=dict( size=(288, 288),
                                                        crop=True),
                                    null=True,
    )
    date_of_birth = models.DateField(u'Дата рождения',null=True)
    post = models.CharField(u'Должность',max_length=255, blank=True)
    department = models.ForeignKey('main.CompanyStructure',
                                    related_name='dep_users',
                                    null=True, blank=True,
                                    verbose_name=u'Отдел')
    phone = models.CharField(u'Дом. телефон',max_length=255, blank=True)
    cell_phone = models.CharField(u'Моб. телефон',max_length=255, blank=True)
    office_phone = models.CharField(u'Раб. телефон',max_length=255, blank=True)
    housing = models.CharField(u'Корпус', max_length=255, blank=True)
    office = models.CharField(u'Офис', max_length=255, blank=True)
    USERNAME_FIELD = 'username'
    
    @property
    def get_full_name(self):
        full_name = '{0} {1}'.format(self.first_name, self.last_name)
        if self.post:
            full_name = '{0} {1}'.format(full_name, self.post)
        return full_name.strip() or self.username

    @property
    def get_full_name_without_post(self):
        full_name = '{0} {1}'.format(self.first_name, self.last_name)
        return full_name.strip() or self.username

    @property
    def small_avatar(self):
        opts = dict(size=(50,50))
        try:
            thumbnail = get_thumbnailer(self.avatar).get_thumbnail(opts)
            return thumbnail.url
        except: pass

    @property
    def coworkers(self):
        if self.department:
            return self.department.dep_users.exclude(pk=self.pk)

    def get_absolute_url(self):
        return reverse('accounts:profile', kwargs=dict(pk=self.pk))

    class Meta:
        ordering=('-date_joined',)