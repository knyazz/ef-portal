from django.conf.urls import patterns, url

urlpatterns = patterns('accounts.views',
    url(r'^new_users/$', 'new_users', name='new_users'),
    url(r'^on_vacation_users/$', 'on_vacation_users', name='on_vacation_users'),
    url(r'^birthday_users/$', 'birthday_users', name='birthday_users'),
    url(r'^profile/(?P<pk>\d+)/$', 'profile', name='profile'),


    url(r'^userdetails/(?P<pk>\d+)/$', 'userdetails', name='userdetails'),
)