# -*- coding: utf-8 -*-

from main.tests import BaseTest


class AccountsTest(BaseTest):
    def accounts_simple_tests(self):
        '''
            test accounts functionality
        '''
        url_lst = ( 'accounts:new_users',
                    'accounts:on_vacation_users',)
        self.get_simple_pages(url_lst)

        self.get_simple_page('accounts:profile', kw=dict(pk=1))
        #self.get_simple_page('accounts:userdetails', kw=dict(pk=1))