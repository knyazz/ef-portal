#coding: utf-8
from django.contrib.auth import get_user_model
User = get_user_model()

from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    small_avatar = serializers.Field(source='small_avatar')
    date_of_birth = serializers.DateField(format="%d.%m.%Y",
                                          input_formats=["%d.%m.%Y"])
    class Meta:
        model = User
        fields = ('avatar', 'small_avatar',
                    'first_name',
                    'last_name',
                    'email',
                    'date_of_birth',
                    'phone',
                    'cell_phone',
                    'office_phone',
                    'post',
                    'housing',
                    'office'
            )