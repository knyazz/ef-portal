#coding: utf-8
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
User = get_user_model()
from django.db.models import Q
from django.views.generic import UpdateView

from rest_framework import generics

from main.models import Vacation, BirthDate
from main.views import ListPage

from .forms import UserForm, UsrAVAForm
from .permissions import IsSelfUser
from .serializers import UserSerializer


class ProfileDetails(UpdateView):
    context_object_name = 'profile'
    model = User
    template_name = 'profile.html'
    form_class = UsrAVAForm

    def post(self, *args, **kwargs):
        if self.request.user == self.get_object():
            return super(ProfileDetails, self).post(*args, **kwargs)
        return self.get_success_url()

    def get_success_url(self):
        return reverse('accounts:profile', kwargs=dict(pk=self.request.user.pk))

    def get_context_data(self, **kwargs):
        ctx = super(ProfileDetails, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            ctx['usrform']= UserForm(instance=self.request.user)
        return ctx
profile = ProfileDetails.as_view()

class UserList(ListPage):
    model = User


class NewUsers(UserList):
    template_name='new_users.html'
    queryset=User.objects.new_users()

    def get_queryset(self):
        qs = super(NewUsers, self).get_queryset()
        #TODO: refact this
        q = Q()
        if self.request.GET:
            body = self.request.GET
            if body.get('keyword'):
                fld = body.get('keyword')
                flds = fld.split(' ')
                first = True
                for fld in flds:
                    cond1 = (   Q(first_name__isnull=False) &
                                Q(first_name__icontains=fld)
                    )
                    cond2 = (   Q(last_name__isnull=False) &
                                Q(last_name__icontains=fld)
                    )
                    cond3 = (   Q(first_name__isnull=True) &
                                Q(last_name__isnull=True) &
                                Q(username__icontains=flds[0])
                            )
                    cond4 = Q(post__icontains=fld)
                    if first:
                        q = cond1 | cond2 | cond3 | cond4
                    else:
                        q|= cond1 | cond2 | cond3 | cond4
                    first = False
        res = set(qs.filter(q).values_list('pk', flat=True))
        return qs.filter(pk__in=res)
new_users = NewUsers.as_view()

birthday_users = UserList.as_view(
                            template_name='birthday_users.html',
                            queryset=BirthDate.objects.birth_users()
)

on_vacation_users = ListPage.as_view(
                            template_name='on_vacation_users.html',
                            model= Vacation,
                            queryset=Vacation.objects.on_vacation_users()
)


class UserDetail(generics.RetrieveUpdateAPIView):
    serializer_class = UserSerializer
    model = User
    permission_classes = IsSelfUser,

    def post(self, request, *args, **kwargs):
        return self.patch(request, *args, **kwargs)
userdetails = UserDetail.as_view()