from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DefaultUserAdmin
from django.utils.translation import ugettext_lazy as _

from .forms import UsrChngForm, UsrCrtForm
from .models import User


class UserAdmin(DefaultUserAdmin):
    form = UsrChngForm
    add_form = UsrCrtForm
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': (
                                            'first_name',
                                            'last_name',
                                            'email',
                                            'date_of_birth',
                                            'department',
                                            'phone',
                                            'cell_phone',
                                            'office_phone',
                                            'avatar',
                                            'post',
                                            'housing',
                                            'office'
                                )}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ( 'username', 'password1', 'password2',
                        'first_name',
                        'last_name',
                        'email',
                        'date_of_birth',
                        'department',
                        'phone',
                        'cell_phone',
                        'office_phone',
                        'avatar',
                        'post',
                        'housing',
                        'office'
                    ),
        }),
    )
    list_filter = ( 'is_staff', 'is_superuser', 'is_active', 'groups',
                    'first_name', 'last_name', 'email')
admin.site.register(User, UserAdmin)