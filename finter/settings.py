#coding: utf-8
import os
LOGIN_REDIRECT_URL = '/'
ALLOWED_HOSTS = []

AUTH_USER_MODEL = 'accounts.User'

AUTHENTICATION_BACKENDS = (
    'django_auth_ldap.backend.LDAPBackend',
    'django.contrib.auth.backends.ModelBackend',
)

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

LOCALHOST = '192.168.100.5'

CONVERSEJS_BOSH_SERVICE_URL = 'https://{0}:5280/http-bind'.format(LOCALHOST)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

DEBUG = True
TEMPLATE_DEBUG = True

SECRET_KEY = 'jb7=&3eq!-4oj1-d2((eeub7h*x77u-8-^vm6orjo4#(!syxa)'

# Application definition
INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'bootstrap3',
    'ckeditor',
    'conversejs',
    'filer',
    'easy_thumbnails',
    'embed_video',
    'mathfilters',
    'mptt',
    'rest_framework',
    'suit_ckeditor',  
    'south',
    'widget_tweaks',

    'accounts',
    #'gallery',
    'filestorage',
    'infos',
    'main', #site base main content

    'django_auth_ldap',
)

LANGUAGE_CODE = 'ru'
LANGUAGES = (('ru','Russian'),)
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'finter.urls'

SUIT_CONFIG = {
    'ADMIN_NAME': 'F-International',
    'CONFIRM_UNSAVED_CHANGES': True,
    'MENU_EXCLUDE': ('auth', 'sites'),
    'MENU': (
        {'app': 'main', 'label': u'Общее',},
        {'app': 'accounts', 'label': u'Пользователи',},
        {'app': 'filestorage', 'label': u'Файлы',},
        #{'app': 'gallery', 'label': u'Медиа',},
        {'app': 'infos', 'label': u'О компании',},
    ),
}

CKEDITOR_TOOLBAR = (
    [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templaates' ],
    [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','aRedo' ],
    [ 'Find','Replace','-','SelectAll','-','SpellChecker'],
    [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ],
    [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidaiRtl' ],
    [ 'Link','Unlink','Anachor' ],
    [ 'Image', 'Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBareak' ],
    #[ 'Styles','Format','Font','FontaSize' ],
    [ 'TextColor','BGColor' ],
    [ 'Maximize', 'ShowBlocks'],
)

CKEDITOR_CONFIGS = {
    'default': {
        #'extraPlugins': 'link',
        'toolbar': CKEDITOR_TOOLBAR,
        'height': 'auto',
        'width': 'auto',
    },
}

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'
CKEDITOR_UPLOAD_PATH = 'uploads/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, "templates"),
)

WSGI_APPLICATION = 'finter.wsgi.application'

SOUTH_MIGRATION_MODULES = {
        'easy_thumbnails': 'easy_thumbnails.south_migrations',
    }

#AD LDAP
import ldap
from django_auth_ldap.config import LDAPSearch, LDAPSearchUnion

ldap.protocol_version = 3
AUTH_LDAP_SERVER_URI = "ldap://192.168.5.8:389"

AUTH_LDAP_CONNECTION_OPTIONS = {
    ldap.OPT_REFERRALS: 0,
    ldap.OPT_X_TLS_REQUIRE_CERT: 0,
}

AUTH_LDAP_BIND_DN = "CN=intranet_user,OU=Служебные,OU=ЭФ-Интер,DC=expoforum,DC=ru"
AUTH_LDAP_BIND_PASSWORD = "Hkylpiw2"
AUTH_LDAP_USER_SEARCH = LDAPSearchUnion(
    LDAPSearch( "OU=VO Office,OU=Экспофорум,DC=expoforum,DC=ru",
                ldap.SCOPE_SUBTREE, "(sAMAccountName=%(user)s)"
    ),
    LDAPSearch( "OU=Ленэкспо,OU=Экспофорум,DC=expoforum,DC=ru",
                ldap.SCOPE_SUBTREE, "(sAMAccountName=%(user)s)"
    ),
    LDAPSearch( "OU=Служебные,OU=ЭФ-Интер,DC=expoforum,DC=ru",
                ldap.SCOPE_SUBTREE, "(sAMAccountName=%(user)s)"
    ),
)
AUTH_LDAP_USER_ATTR_MAP = { 
                            "first_name": "givenName", 
                            "last_name": "sn",
                            "email": "mail",
                            "username": "sAMAccountName",
                            "phone": "telephoneNumber",
                        }
AUTH_LDAP_ALWAYS_UPDATE_USER = True

import logging

logger = logging.getLogger('django_auth_ldap')
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)

SOUTH_TESTS_MIGRATE = False
TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
NOSE_ARGS = [#'--with-notify',
             '--no-start-message',
             '--verbosity=2',
             '--with-fixture-bundling',
             ]

try:
    from local_settings import *
except ImportError:
    pass